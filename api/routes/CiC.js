const express = require('express');
const router = express.Router({});
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017";
const auth = require('./auth.js');

const GATEWAYS_COLLECTION = "gateways";
const SESSIONS_COLLECTION = "sessions";

const app = require('express')();
const DB_NAME = "team11";//team11

//returns all heartbeats of gid in database as json
router.get('/heartbeat', async function (req, res) {
    const sessionId = req.get('sessionId');
    const gwToken = req.get('gwToken');

    if(!(await verification(sessionId, gwToken))){
            res.status(401);
            res.json({
                'status': 'FAILED VERIFICATION'
            });

    } else {
       MongoClient.connect(url, { useNewUrlParser: true}, function(err, db) {
           if (err) throw err;
           const dbo = db.db(DB_NAME);

            //finds heartbeats
            query = { "gateway_id": gwToken };

            dbo.collection("heartbeats").find(query).toArray(function(err, result) {
                if (err) throw err;
                res.status(200);
                res.json(result);
                db.close();
              });


        });
    }


});

//returns all diagnostics of gid in database as json
router.get('/diagnostics', async function (req, res) {
    const sessionId = req.get('sessionId');
    const gwToken = req.get('gwToken');

   if(!(await verification(sessionId, gwToken))){
        res.status(401);
        res.json({
            'status': 'FAILED VERIFICATION'
        });

   }else{
      MongoClient.connect(url, { useNewUrlParser: true}, function(err, db) {
          if (err) throw err;
          const dbo = db.db(DB_NAME);

           //finds diagnostics
           query = { "gateway_id": gwToken };

           dbo.collection("diagnostics").find(query).toArray(function(err, result) {
               if (err) throw err;
               res.status(200);
               res.json(result);
               db.close();
             });
       })
  }

});

//returns the code for a given diagnostic test that is stored in the 'diagnostic_code' collection in mongo
router.get('/diagnosticTest', async function (req, res){
    const gwToken = req.get('gwToken');
    const diagName = req.get('diagName');

    if (!(await gwExist(gwToken))){
        res.status(401);
        res.json({
            'status': 'FAIL - gwToken NOT WITHIN DATABASE'
        });
    }else{
      MongoClient.connect(url, { useNewUrlParser: true}, function(err, db) {
          if (err) throw err;
          const dbo = db.db(DB_NAME);

           //finds diagnostics
           query = { "gateway_id": gwToken,
                     "diagnostic_name": diagName
                   };

           dbo.collection("diagnostic_code").find(query).toArray(function(err, result) {
               if (err) throw err;
               res.status(200);
               res.send(result[0].diagnostic_code);
               db.close();
             });
       });
    }

});

//stores heartbeat in mongo collection and checks for any pending diagnostic requests
router.post('/heartbeat', async function(req, res) {
    const gwToken = req.get('gwToken');
    const timestamp = new Date().getTime(); //timestamp is added to heartbeat within the api

    if (!(await gwExist(gwToken))) {
        res.status(401);
        res.json({
            'status': 'FAIL - gwToken NOT WITHIN DATABASE'
        });
    }
    else{
       var obj = {
            "gateway_id": gwToken,
            "timestamp": timestamp
       };

        MongoClient.connect(url, { useNewUrlParser: true}, function(err, db) {
            if (err) throw err;
            const dbo = db.db(DB_NAME);

            dbo.collection("heartbeats").insertOne(obj, function(err, res) {
                if (err) throw err;
                db.close();
            });
        });

        //check if there are any pending diagnostic requests in database
         MongoClient.connect(url, { useNewUrlParser: true}, function(err, db) {
            if (err) throw err;
            const dbo = db.db(DB_NAME);
            dbo.collection("diagnosticReq").findOne({"gateway_id": gwToken}, function(err, result) {
                if(result != null)
                {
                    console.log(result);

                    //deletes first pending request of gid that appears in database
                    try{
                     dbo.collection("diagnosticReq").deleteOne({"gateway_id" : gwToken}, function(err, obj) {
                          if (err) throw err;
                          console.log("1 document deleted");

                     });
                    } catch (e) {
                        print (e);
                     }

                    //close and reply with diagnostic request
                    db.close();
                    res.status(201);
                    res.json({
                        'status': 'INSERTED',
                        'diagnostic': result.diagnostic_name,
                        'scheduled': result.scheduled
                    });
                }
                else{
                    //close and reply
                    db.close();
                    res.status(201);
                    res.json({
                        'status': 'INSERTED'
                    });
                }

            });


         });

    }

});

//stores diagnostic info in mongo collection
router.post('/diagnostics', async function(req, res) {
    const gwToken = req.get('gwToken');
    const diag = req.get('diag');

    if (!(await gwExist(gwToken))) {
        res.status(401);
        res.json({
            'status': 'FAIL - gwToken NOT WITHIN DATABASE'
        });
    }
    else{
        var obj = {
            "gateway_id": gwToken,
            "diagnostics": diag
        }

        MongoClient.connect(url, { useNewUrlParser: true}, function(err, db) {
            if (err) throw err;
            const dbo = db.db(DB_NAME);

            dbo.collection("diagnostics").insertOne(obj, function(err, res) {
                if (err) throw err;
                db.close();
            })
        })
        res.status(201);
        res.json({
            'status': 'INSERTED'
        });

    }

});

//stores diagnostic request to gateway in mongo collection
router.post('/diagnosticReq', async function(req, res) {
    const sessionId = req.get('sessionId');
    const gwToken = req.get('gwToken');
    const diagName = req.get('diagName');
    const scheduled = req.get('scheduled');

      if(!(await verification(sessionId, gwToken))){
           res.status(401);
           res.json({
               'status': 'FAILED VERIFICATION'
           });

      }
      else{
            MongoClient.connect(url, { useNewUrlParser: true}, function(err, db) {
                if (err) throw err;
                const dbo = db.db(DB_NAME);

              //inserts request
              const obj = {
                  "gateway_id": gwToken,
                  "diagnostic_name": diagName,
                  "scheduled": scheduled
              }

              dbo.collection("diagnosticReq").insertOne(obj, function(err, res) {
                  if (err) throw err;
                  db.close();
              })
            })
            res.status(201);
            res.json({
              'status': 'REQUEST RECEIVED'
            });
      }


})

//stores the body of req in 'diagnostic_code' collection in mongo
router.post('/diagnosticTest', async function (req, res){
    const sessionId = req.get('sessionId');
    const gwToken = req.get('gwToken');
    const diagName = req.get('diagName');

    //extracting info from body
    let body = [];
    req.on('data', (chunk) => {
      body.push(chunk);
    }).on('end', () => {
      body = Buffer.concat(body).toString();
      // at this point, `body` has the entire request body stored in it as a string
    });

      if(!(await verification(sessionId, gwToken))){
           res.status(401);
           res.json({
               'status': 'FAILED VERIFICATION'
           });

      }else{
            MongoClient.connect(url, { useNewUrlParser: true}, function(err, db) {
                if (err) throw err;
                const dbo = db.db(DB_NAME);

              //inserts request
              const obj = {
                  "gateway_id": gwToken,
                  "diagnostic_name": diagName,
                  "diagnostic_code": body
              }

              dbo.collection("diagnostic_code").insertOne(obj, function(err, res) {
                  if (err) throw err;
                  db.close();
              })
            })
            res.status(201);
            res.json({
              'status': 'INSERTED'
            });


      }

});


router.options('/', function (req, res) {
    res.header('Allow', 'GET, POST').status(204).send();
});



console.log("CiC check");
module.exports = router;


async function verification(sessionId, gwToken){
    const accountId = await accountAssociatedWithSession(sessionId);
    const result = await accountAssociatedWithGW(accountId, gwToken);
    return result;
}

//Checks that the aid (account id) associated with the given sessionId
async function accountAssociatedWithSession(sessionId){

    return  MongoClient.connect(url, { useNewUrlParser: true}).then((db) => {
            const dbo = db.db(DB_NAME);

            const query = {
                "sid" : sessionId
            };
            return dbo.collection(SESSIONS_COLLECTION).find(query).toArray().then((result) => {
               if (result.length == 1) {
                   db.close();
                   return result[0].aid;
               } else {
                    db.close();
                    return null;
               }
           });
    });
}


// Checks whether to see that the given gwToken is for the given account id
// and that gwToken is not null
async function accountAssociatedWithGW(accountId, gwToken){
    if (gwToken === null) {
        return false;
    }
    else{
        if (accountId === null) {
            return false;
        }

        return MongoClient.connect(url, { useNewUrlParser: true}).then((db) => {
           const dbo = db.db(DB_NAME);

           const query = {
               "aid" : accountId,
               "token" : gwToken
           }
           return dbo.collection(GATEWAYS_COLLECTION).find(query).toArray().then((result) => {
                if(result.length == 1){
                     db.close();
                     return true;
                } else{
                    db.close();
                    return false;
                }
                throw Error("system has reached unreachable code in accountAssociatedWithSession. Thats no good");
            })
       })
    }
}

//checks if gwToken exists in the "gateways" collection in mongo
async function gwExist(gwToken){
    if (gwToken == null) {
        return false;
    }else{
        return MongoClient.connect(url, { useNewUrlParser: true}).then((db) => {
            const dbo = db.db(DB_NAME);

            var query = {
                "token" : gwToken
            }
            return dbo.collection(GATEWAYS_COLLECTION).find(query).toArray().then((result) => {
                if(result.length == 1){
                     //console.log('true');
                     db.close();
                     return true;
                 } else{
                    //console.log('false');
                    db.close();
                    return false;
                 }
                 throw Error("system has reached unreachable code in gwExist. Thats no good");
            })
        })
    }

}