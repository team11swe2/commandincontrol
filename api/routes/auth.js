const express = require('express');
var bcrypt = require('bcryptjs');
const MongoClient = require('mongodb').MongoClient;

const router = express.Router({});
const url = "mongodb://localhost:27017/";

const app = require('express')();


const _id = "_id";

const DB_NAME = "team11";
const ACCOUNTS_COLLECTION = "accounts";
const password = "password";
const username = "username";
const firstname = "firstname";
const lastname = "lastname";
const email = "email";

const SESSIONS_COLLECTION = "sessions";
const aid = "aid";
const sid = "sid";

const GATEWAYS_COLLECTION = "gateways"
const token = "token"
// contains aid too

router.post('/signup', function(req, res) {

	const givenUsername = req.get(username);
	const givenFirstName = req.get(firstname);
	const givenLastName = req.get(lastname);
	const givenEmail = req.get(email);
	var unhashedPassword = req.get(password);


	if (givenUsername == null || givenFirstName == null || givenLastName == null || givenEmail == null || unhashedPassword == null) {
		res.status(400)
		res.json(false)
		return
	}

	const hashedPassword = bcrypt.hashSync(unhashedPassword, bcrypt.genSaltSync(10));


	const singupObj = {
		username : givenUsername,
		firstname : givenFirstName,
		lastname : givenLastName,
		email : givenEmail,
		password : hashedPassword
	}

	MongoClient.connect(url, { useNewUrlParser: true}, function(err, db) {
		if (err) throw err;
		const dbo = db.db(DB_NAME);
		const checkSingup = {
			username : givenUsername
		}

		dbo.collection(ACCOUNTS_COLLECTION).find(checkSingup).toArray(function(err, result) {
			if (err) throw err;
			if (result.length > 0) {
				res.status(400);
				res.json("username " + givenUsername + " already exists...");
				return;
			} else {
				dbo.collection(ACCOUNTS_COLLECTION).insertOne(singupObj, function(err, result) {
					if (err) throw err;
					db.close();
					res.status(200);
					res.json(true);
					return;
				})
			}
		})
	})
})

router.post('/login', function(req, res) {
	const givenUsername = req.get(username);
	const givenPassword = req.get(password);

	if (givenUsername == null || givenPassword == null) {
		res.status(400);
		res.json(false);
		return;
	}

	MongoClient.connect(url, { useNewUrlParser: true}, function(err, db) {
		if (err) throw err;
		const dbo = db.db(DB_NAME);
		const loginInfo = {
			username : givenUsername
		}
		dbo.collection(ACCOUNTS_COLLECTION).find(loginInfo).toArray(function(err, result) {
			if (err) throw err;

			if (result.length >= 1) {
				const account = result[0];

				if (bcrypt.compareSync(givenPassword, account.password)) {
					dbo.collection(SESSIONS_COLLECTION).deleteMany({ aid : account._id}, function(err, obj) {
					    if (err) throw err;
						var sid = randomString();
						const sessionObj = {
							aid : account._id,
							sid : sid
						}
						dbo.collection(SESSIONS_COLLECTION).insertOne(sessionObj, function(err, result) {
							if (err) throw err;
							db.close();
							res.status(200);
							res.json({ "sid" : sid });
							return;
						})
					  });
				} else {
					res.status(422);
					res.json(false);
					return;
				}
			} else {
				res.status(422);
				res.json(false);
				return;

			}
		})
	})
})

router.get('/newgateway', function(request, response) {
	const givenSid = request.get(sid)

	MongoClient.connect(url, { useNewUrlParser: true}, function(err, db) {
		if (err) throw err
		const dbo = db.db(DB_NAME)
		const query = {
			sid : givenSid
		}
		dbo.collection(SESSIONS_COLLECTION).find(query).toArray(function(err, result) {
			if (result.length == 1) {
				const gatewayToken = randomString()
				const newGateway = {
					aid : result[0].aid,
					token : gatewayToken
				}
				dbo.collection(GATEWAYS_COLLECTION).insertOne(newGateway, function(err, result) {
					if (err) throw err
					response.status(200);
					response.json({
						token : gatewayToken
					})
					db.close();

				});
			} else {
				response.status(422) //TODO: Change to authorization failure status code
				response.json(false)
				db.close();

			}
		})
	})
})

router.get("/gateways", function(request, response) {
	const givenSid = request.get(sid)

	MongoClient.connect(url, { useNewUrlParser: true}, function(err, db) {
		if (err) throw err
		const dbo = db.db(DB_NAME)
		const query = {
			sid : givenSid
		}
		dbo.collection(SESSIONS_COLLECTION).find(query).toArray(function(err, result) {
			if (result.length == 1) {
				const gatewayToken = randomString()
				const gateways = {
					aid : result[0].aid,
				}
				dbo.collection(GATEWAYS_COLLECTION).find(gateways).toArray(function(err, result) {
					if (err) throw err
					var rv = []
					for (i = 0; i < result.length; i++) {
    					rv.push(result[i].token)
					}
					response.status(200);
					response.json(rv)
					return
				});
			} else {
				response.status(422) //TODO: Change to authorization failure status code
				response.json(false)
				return
			}
		})
	})
})



router.options('/', function (req, res) {
    res.header('Allow', 'POST').status(204).send();
});

module.exports = router;
//exports.verifyGatewayTokenOwner;
//exports.authenticateSession;

// Checks whether to see that the given gwToken is for the given account id
module.exports.verifyGatewayTokenOwner = function (accountId, gwToken) {
	MongoClient.connect(url, { useNewUrlParser: true}, function(err, db) {
		if (err) throw err
		const dbo = db.db(DB_NAME)
		const query = {
			aid : accountId,
			token : gwToken
		}
		dbo.collection(GATEWAYS_COLLECTION).find(query).toArray(function(err, result) {
			return result.length == 1
		})
	})
}

// Returns the aid (account id) associated with the given sessionId
module.exports.authenticateSession = function (sessionId) {
	MongoClient.connect(url, { useNewUrlParser: true}, function(err, db) {
		if (err) throw err
		const dbo = db.db(DB_NAME)
		const query = {
			sid : sessionId
		}
		dbo.collection(SESSIONS_COLLECTION).find(query).toArray(function(err, result) {
			if (result.length == 1) {
				return result[0].aid
			} else {
				return null
			}
		})
	})
}

function randomString() {
	var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
	var string_length = 64;
	var randomstring = '';
	for (var i=0; i<string_length; i++) {
		var rnum = Math.floor(Math.random() * chars.length);
		randomstring += chars.substring(rnum,rnum+1);
	}
	return randomstring;
}
